package com.ezekiel.biblioteca.commons.dto;

import java.io.Serializable;

public class LibroEstadoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int idLibro;
	private String isbn;
	private String titulo;
	private String autor;
	private String categoria;
	private boolean disponible;
	
	public LibroEstadoDTO() {}

	public int getIdLibro() {
		return idLibro;
	}

	public void setIdLibro(int idLibro) {
		this.idLibro = idLibro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}	
}
