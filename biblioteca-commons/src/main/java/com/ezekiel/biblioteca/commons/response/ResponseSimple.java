package com.ezekiel.biblioteca.commons.response;

public class ResponseSimple<E> extends Response {
	private E item;	

	public ResponseSimple() {}

	public ResponseSimple(E item) {
		this.item = item;
	}

	public E getItem() {
		return item;
	}

	public void setItem(E item) {
		this.item = item;
	}	
}
