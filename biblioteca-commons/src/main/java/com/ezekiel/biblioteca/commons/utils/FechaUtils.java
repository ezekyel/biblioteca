package com.ezekiel.biblioteca.commons.utils;

import java.util.Calendar;
import java.util.Date;

public class FechaUtils {
	public static Date getFechaHoy(){
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getTime();
	}

	public static Date sumarDiasFechaHoy(int dias){
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, dias);
		return calendar.getTime();
	}
}
