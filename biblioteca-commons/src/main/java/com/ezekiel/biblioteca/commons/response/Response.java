package com.ezekiel.biblioteca.commons.response;

public abstract class Response {
	private String menssage;
	private String errorMessage;
	public final static String SUCCESS = "Success";
	public final static String ERROR = "Error";
	
	public String getMenssage() {
		return menssage;
	}
	public void setMenssage(String menssage) {
		this.menssage = menssage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}	
}
