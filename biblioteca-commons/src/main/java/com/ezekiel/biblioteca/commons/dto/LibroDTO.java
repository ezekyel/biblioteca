package com.ezekiel.biblioteca.commons.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

public class LibroDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int idLibro;
	
	@NotEmpty
	private String isbn;
	
	@NotEmpty
	private String titulo;

	@NotEmpty
	private String autor;

	@NotEmpty
	private String categoria;

	public LibroDTO() {}

	public int getIdLibro() {
		return idLibro;
	}
	
	public void setIdLibro(int idLibro) {
		this.idLibro = idLibro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}
