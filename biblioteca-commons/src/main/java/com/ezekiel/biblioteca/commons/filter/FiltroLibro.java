package com.ezekiel.biblioteca.commons.filter;

public class FiltroLibro extends Filtro {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idLibro;	
	private String isbn;
	private String titulo;
	private String autor;
	private String categoria;
	
	public FiltroLibro() {}

	public Integer getIdLibro() {
		return idLibro;
	}
	
	public void setIdLibro(Integer idLibro) {
		this.idLibro = idLibro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}
