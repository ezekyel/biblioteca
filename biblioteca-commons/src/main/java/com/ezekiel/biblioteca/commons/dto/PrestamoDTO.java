package com.ezekiel.biblioteca.commons.dto;

import java.io.Serializable;
import java.util.Date;

import com.ezekiel.biblioteca.domain.Libro;
import com.ezekiel.biblioteca.domain.Socio;

public class PrestamoDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer idPrestamo;		
	private Date fechaPrestamo;
	private Date fechaDevolucion;
	private String estado;
	private Socio socio;
	private Libro libro;
	
	public PrestamoDTO(){}

	public Integer getIdPrestamo() {
		return idPrestamo;
	}

	public void setIdPrestamo(Integer idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public Date getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(Date fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}
	
}
