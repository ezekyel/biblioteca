package com.ezekiel.biblioteca.commons.response;

import java.util.List;

public class ResponsePage<T> extends Response {
	private Integer page;
	private Integer pageSize;
	private Integer lastPage;
	private Integer totalCount;
	private List<T> items;
	
	public ResponsePage() {}
	
	public ResponsePage(Integer page, Integer pageSize, Integer totalCount) {
		this.page = page;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
		
		int paginasEnteras = totalCount / pageSize;
		int paginasResto = totalCount % pageSize;
		this.lastPage = paginasEnteras + (paginasResto == 0 ? 0 : 1);
	}

	public Integer getPage() {
		return page;
	}
	
	public void setPage(Integer page) {
		this.page = page;
	}
	
	public Integer getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	public Integer getLastPage() {
		return lastPage;
	}
	
	public void setLastPage(Integer lastPage) {
		this.lastPage = lastPage;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}
	
	public void setLista(List<T> lista) {
		this.items = lista;
	}
	
	public Integer firstItem() {
		return page <= 1 ? 0 : ((page -1 )* pageSize);
	}
}
