<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />	

<title>Buscar ficha socioe</title>
</head>
<body>
	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Buscar socio</h3>
		</div>
			
		<div class="panel-body">

			<div class="centrar ">
				<form action="buscarSocio.html" class="navbar-form navbar-left">
					<div class="form-group">
						<label for="dni">DNI</label> <input name="dni" id="dni"
							class="form-control" value="${dni}" />
					</div>

					<button type="submit" class="btn btn-primary">Buscar</button>

				</form>

			</div>
		</div>
	</div>
</body>
</html>