<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Lista de socios</title>
</head>
<body>

	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Listado de Socios</h3>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>DNI</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Direccion</th>
						<th>Email</th>
						<th>Editar</th>
						<th>Eliminar</th>
					</tr>
				</thead>

				<c:forEach items="${socios}" var="socio">
					<tr>
						<td>${socio.dni}</td>
						<td>${socio.nombre}</td>
						<td>${socio.apellido}</td>
						<td>${socio.direccion}</td>
						<td>${socio.email}</td>

						<td><a href="editarSocio?id=${socio.idSocio}"><span
								class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="borrarSocio?id=${socio.idSocio}"><span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</c:forEach>

			</table>
		</div>
	</div>
</body>
</html>