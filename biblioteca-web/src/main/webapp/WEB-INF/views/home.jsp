<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Biblioteca</title>
</head>
<body>
	<h1>Biblioteca</h1>
	<%@ include file="fragments/menu.jsp"%>

	<div class="jumbotron">
		<h1>Biblioteca digital</h1>
	</div>
</body>
</html>