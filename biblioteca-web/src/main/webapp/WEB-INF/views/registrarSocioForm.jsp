<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Registro del libros</title>
</head>
<body>

	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Registro de socio</h3>
		</div>

		<div class="panel-body">
			<form:form action="guardarSocio.html" modelAttribute="socioDTO"
				cssClass="form-horizontal">

				<form:hidden path="idSocio" value="${socioDto.idSocio}" />

				<spring:bind path="dni">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="dni" cssClass="col-sm-2 control-label">DNI</form:label>
						<div class="col-xs-4">
							<form:input path="dni" value="${socioDto.dni}"
								cssClass="form-control" />
							<form:errors path="dni" />
						</div>
					</div>
				</spring:bind>
				
				<spring:bind path="nombre">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="nombre" cssClass="col-sm-2 control-label">Nombre</form:label>
						<div class="col-xs-4">
							<form:input path="nombre" value="${socioDto.nombre}"
								cssClass="form-control" />
							<form:errors path="nombre" />
						</div>
					</div>
				</spring:bind>
				
				<spring:bind path="apellido">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="apellido" cssClass="col-sm-2 control-label">Apellido</form:label>
						<div class="col-xs-4">
							<form:input path="apellido" value="${socioDto.apellido}"
								cssClass="form-control" />
							<form:errors path="apellido" />
						</div>
					</div>
				</spring:bind>
				
				<spring:bind path="direccion">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="direccion" cssClass="col-sm-2 control-label">Direccion</form:label>
						<div class="col-xs-4">
							<form:input path="direccion" value="${socioDto.direccion}"
								cssClass="form-control" />
							<form:errors path="direccion" />
						</div>
					</div>
				</spring:bind>

				<spring:bind path="telefono">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="telefono" cssClass="col-sm-2 control-label">Telefono</form:label>
						<div class="col-xs-4">
							<form:input path="telefono" value="${socioDto.telefono}"
								cssClass="form-control" />
							<form:errors path="telefono" />
						</div>
					</div>
				</spring:bind>
				
				<spring:bind path="email">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="email" cssClass="col-sm-2 control-label">Email</form:label>
						<div class="col-xs-4">
							<form:input path="email" value="${socioDto.email}"
								cssClass="form-control" />
							<form:errors path="email" />
						</div>
					</div>
				</spring:bind>

				<div class="col-sm-offset-5">
					<input type="submit" value="Guardar" class="btn btn-primary">
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>