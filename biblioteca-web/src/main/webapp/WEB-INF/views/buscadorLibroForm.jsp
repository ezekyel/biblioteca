<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />
	
<title>Buscar libro</title>
</head>
<body>
	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Buscar Libro</h3>
		</div>
	</div>

	<div class="panel-body">
		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-inline" action="buscar.html">
					<div class="form-group">
						<select id="buscarPor" name="buscarPor" class="form-control">
							<option value="" selected="selected">Buscar por</option>
							<option value="isbn">ISBN</option>
							<option value="titulo">Titulo</option>
							<option value="autor">Autor</option>
							<option value="Categoria">Categoria</option>
						</select>
					</div>
					<div class="form-group">
						<label for="exp" class="sr-only"></label> <input type="text"
							class="form-control" id="exp" name="exp"
							placeholder="Escriba el texto a buscar">
					</div>
					<button type="submit" class="btn btn-primary">Buscar</button>
				</form>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading"><h4>Libros encontrados</h4></div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ISBN</th>
							<th>Titulo</th>
							<th>Autor</th>
							<th>Categoria</th>
							<th>Dinsponible</th>
							<th>Aceptar</th>
						</tr>
					</thead>

					<c:forEach items="${libros}" var="libro">
						<tr>
							<td>${libro.isbn}</td>
							<td>${libro.titulo}</td>
							<td>${libro.autor}</td>
							<td>${libro.categoria}</td>
							<td>${libro.disponible == true ? 'Si' : 'No'}</td>

							<td>
								<c:choose>
									<c:when test="${libro.disponible}">
										<a href="aceptarLibro.html?idLibro=${libro.idLibro}">
											<span class="glyphicon glyphicon-ok"></span></a>								
									</c:when>
									<c:otherwise>
										<a href="" disabled="disabled">
											<span class="glyphicon glyphicon-ok"></span></a>										
									</c:otherwise>								
								</c:choose>
							</td>
						</tr>
					</c:forEach>

				</table>
			</div>
		</div>
	</div>
</body>
</html>