<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Ficha del socio</title>
</head>
<body>
	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Socio: ${socio.nombre} ${socio.apellido}</h3>
		</div>
	</div>

	<div class="panel-body">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Registrar prestamo</h4>
			</div>
			<div class="panel-body">

				<div class="row">

					<div class="col-xs-12 col-md-8">

						<div class="panel panel-default">
							<div class="panel-body">
								<form class="form-horizontal">
									<label class="col-sm-2 control-label">ISBN</label>
									<div class="col-sm-10">
										<p class="form-control-static">${libro.isbn}</p>
									</div>

									<label class="col-sm-2 control-label">Titulo</label>
									<div class="col-sm-10">
										<p class="form-control-static">${libro.titulo}</p>
									</div>

									<label class="col-sm-2 control-label">Autor</label>
									<div class="col-sm-10">
										<p class="form-control-static">${libro.autor}</p>
									</div>

									<label class="col-sm-2 control-label">Categoria</label>
									<div class="col-sm-10">
										<p class="form-control-static">${libro.categoria}</p>
									</div>
								</form>

							</div>
						</div>
					</div>

					<div class="col-xs-7 col-md-2">
						<p>
							<a class="btn btn-primary btn-lg btn-block"
								href="buscarLibro.html?id=${socio.idSocio}" >Buscar libro</a>
						</p>
						<p>
							<a class="btn btn-primary btn-lg btn-block"								
								${libro.isbn == null 
									? 'href=\"\" disabled=\"disabled\"' 
									: 'href=\"agregarLibro.html?idlibro=libro.idLibro\"'}>Agregar libro</a>
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Libros en prestamo</h4>
			</div>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>Fecha devolucion</th>
						<th>Titulo</th>
						<th>Autor</th>
						<th>Devolver</th>
					</tr>
				</thead>

				<c:forEach items="${prestamos}" var="prestamo">
					<tr>
						<td>${prestamo.fechaDevolucion}</td>
						<td>${prestamo.libro.titulo}</td>
						<td>${prestamo.libro.autor}</td>

						<td><a href="devolverLibro?id=${prestamo.idPrestamo}"><span
								class="glyphicon glyphicon-book"></span></a></td>
					</tr>
				</c:forEach>

			</table>
		</div>

	</div>

</body>
</html>