<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Listado de libros</title>
</head>
<body>
	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Listado de libros</h3>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Isbn</th>
						<th>Titulo</th>
						<th>Autor</th>
						<th>Categoria</th>
						<th>Editar</th>
						<th>Eliminar</th>
					</tr>
				</thead>

				<c:forEach items="${libros}" var="libro">
					<tr>
						<td>${libro.isbn}</td>
						<td>${libro.titulo}</td>
						<td>${libro.autor}</td>
						<td>${libro.categoria}</td>

						<td><a href="editarLibro?id=${libro.idLibro}"><span
								class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="borrarLibro?id=${libro.idLibro}"><span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</c:forEach>

			</table>
		</div>
	</div>
</body>
</html>