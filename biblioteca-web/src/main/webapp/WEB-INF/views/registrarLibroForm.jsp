<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />
	
<title>Registro del libros</title>
</head>
<body>

	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Registro de libro</h3>
		</div>

		<div class="panel-body">
			<form:form action="guardarLibro.html" modelAttribute="libroDTO"
				cssClass="form-horizontal">

				<spring:bind path="isbn">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="isbn" cssClass="col-sm-2 control-label">ISBN</form:label>
						<div class="col-xs-4">
							<form:hidden path="idLibro" value="${libroDto.idLibro}" />
							<form:input path="isbn" value="${libroDto.isbn}"
								cssClass="form-control" />
							<form:errors path="isbn" />
						</div>
					</div>
				</spring:bind>

				<spring:bind path="titulo">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="titulo" cssClass="col-sm-2 control-label">Titulo</form:label>
						<div class="col-xs-4">
							<form:input path="titulo" value="${libroDto.titulo}"
								cssClass="form-control" />
							<form:errors path="titulo" />
						</div>
					</div>
				</spring:bind>

				<spring:bind path="autor">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="autor" cssClass="col-sm-2 control-label">Autor</form:label>
						<div class="col-xs-4">
							<form:input path="autor" value="${libroDto.autor}"
								cssClass="form-control" />
							<form:errors path="autor" />
						</div>
					</div>
				</spring:bind>

				<spring:bind path="categoria">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="categoria" cssClass="col-sm-2 control-label">Categoria</form:label>
						<div class="col-xs-4">
							<form:input path="categoria" value="${libroDto.categoria}"
								cssClass="form-control" />
							<form:errors path="categoria" />
						</div>
					</div>
				</spring:bind>

				<div class="col-sm-offset-5">
					<input type="submit" value="Guardar" class="btn btn-primary">
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>