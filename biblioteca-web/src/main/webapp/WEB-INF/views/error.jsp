<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isErrorPage="true"%>

<!DOCTYPE html>
<html lang="en">

<head>
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error</title>
</head>
<body>

	<h2>Ha ocurrido un error en la aplicacion :</h2>

	<p><%=exception.getMessage()%></p>
	<p><%=exception.getCause().getMessage()%></p>

</body>
</html>