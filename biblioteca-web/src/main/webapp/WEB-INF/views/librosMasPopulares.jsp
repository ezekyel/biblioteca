<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />

<title>Reportes</title>
</head>
<body>

	<%@ include file="fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Reportes</h3>
		</div>

		<div class="panel-body">

			<%@ include file="fragments/navReportes.jsp" %>

			<table class="table table-hover">
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Autor</th>
						<th>Categoria</th>
					</tr>
				</thead>
				<tbody>				
					<c:forEach items="${libros}" var="libro">
						<tr>
							<td>${libro.titulo}</td>
							<td>${libro.autor}</td>
							<td>${libro.categoria}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
						
		</div>
	</div>

</body>
</html>