<ul class="nav nav-tabs nav-justified">
	<li class="${css == 'librosMasPopulares' ? 'active' : '' }"><a href="librosMasPopulares.html">Libros mas populares</a></li>
	<li class="${css == 'prestamosVencidos'  ? 'active' : '' }"><a href="prestamosVencidos.html">Prestamos vencidos</a></li>
	<li class="${css == 'reportePorLibro'    ? 'active' : '' }"><a href="reportePorLibro.html">Reporte por libros</a></li>
	<li class="${css == 'reportePorSocio'    ? 'active' : '' }"><a href="reportePorSocio.html">Reporte por socios</a></li>
</ul>	