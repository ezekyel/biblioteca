<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Biblioteca</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="listarLibros.html">Listado de libros</a></li>
			<li><a href="registrarLibro.html">Registrar libro</a></li>
			<li><a href="listarSocios.html">Listado de socios</a></li>
			<li><a href="registrarSocio.html">Registrar socio</a></li>
			<li><a href="buscarFichaSocio.html">Registrar prestamos</a></li>
			<li><a href="reportes.html">Reportes</a></li>
		</ul>
	</div>
</nav>