package com.ezekiel.biblioteca.web;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.servicios.ServicioLibro;

@Controller
public class LibroController {
	
	private final Logger logger = LoggerFactory.getLogger(LibroController.class);
	
	@Autowired
	private ServicioLibro servicioLibro;
	
	@RequestMapping("registrarLibro")
	public ModelAndView registrarlibro(@ModelAttribute LibroDTO libro){
		logger.debug("nuevo libro");
		return new ModelAndView("registrarLibroForm");
	}
	
	@RequestMapping("editarLibro")
	public ModelAndView editarLibro(@RequestParam int id, 
									@ModelAttribute LibroDTO libro){
		logger.debug("actulizando libro " + id);
		libro = servicioLibro.buscarLibro(id);
		return new ModelAndView("registrarLibroForm", "libroDto", libro);
	}
	
	@RequestMapping("guardarLibro")
	public ModelAndView guardarLibro(@Valid @ModelAttribute LibroDTO libro, BindingResult result){

		if(result.hasErrors()){
			logger.debug("error al guardar");
			return new ModelAndView("registrarLibroForm");
		} 
		logger.debug("guardado ok");
		servicioLibro.guardarLibro(libro);
		return new ModelAndView("redirect:listarLibros");		
	}
	
	@RequestMapping("borrarLibro")
	public ModelAndView borrarLibro(@RequestParam int id){
		logger.debug("borrado " + id);
		servicioLibro.borrarLibro(id);
		return new ModelAndView("redirect:listarLibros");
	}
	
	@RequestMapping(value = {"listarLibros", "/"})
	public ModelAndView listarLibros(){
		logger.debug("listando libros");
		List<LibroDTO> libros = servicioLibro.buscarTodosLosLibros();
		return new ModelAndView("listaLibros", "libros", libros);
	}
}
