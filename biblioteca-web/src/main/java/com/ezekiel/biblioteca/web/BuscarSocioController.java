package com.ezekiel.biblioteca.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.servicios.ServicioSocio;

@Controller
public class BuscarSocioController {
	
	@Autowired
	private ServicioSocio servicioSocio;
	
	@RequestMapping("buscarFichaSocio")
	public ModelAndView registrarPrestamo() {
		return new ModelAndView("buscarFichaSocio");
	}

	@RequestMapping("buscarSocio")
	public String fichaDelSocio(@RequestParam(value="dni", required=false) String dni,
							     Model model) {	
		int numDni;
		boolean error = false;
		
		try {
			numDni = Integer.parseInt(dni);			
		} catch (Exception e) {
			numDni = 0;
		}
		
		SocioDTO socioDto = servicioSocio.getSocioByDni(numDni);		
		model.addAttribute("error", error);
		
		if(socioDto == null) {			
			error = true;
			return "redirect:buscarFichaSocio";
		}		
		int idSocio = socioDto.getIdSocio();
		model.addAttribute("socio", idSocio);
		return "redirect:fichaSocio?id=" + idSocio;
	}
}
