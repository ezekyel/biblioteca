package com.ezekiel.biblioteca.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.LibroEstadoDTO;
import com.ezekiel.biblioteca.commons.dto.PrestamoDTO;
import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.servicios.ServicioLibro;
import com.ezekiel.biblioteca.servicios.ServicioPrestamo;
import com.ezekiel.biblioteca.servicios.ServicioSocio;

@Controller
@SessionAttributes({"socio", "prestamos","libro"})
public class FichaSocioController {
	
	@Autowired
	private ServicioSocio servicioSocio;
	
	@Autowired
	private ServicioLibro servicioLibro;
	
	@Autowired
	private ServicioPrestamo servicioPrestamo;
	
	@RequestMapping("fichaSocio")
	public String fichaSocio(@RequestParam Integer id, 
							 Model model){		
			
		SocioDTO socio = servicioSocio.getSocioById(id);		
		List<PrestamoDTO> prestamos = servicioPrestamo.getPrestamos(socio);

		LibroDTO libroDto = new LibroDTO();
		model.addAttribute("prestamos", prestamos);
		model.addAttribute("socio", socio);
		model.addAttribute("libro", libroDto);
		return "fichaSocioForm";
	}
	
	@RequestMapping("buscarLibro")
	public String buscarLibro(){
		return "buscadorLibroForm";
	}
	
	@RequestMapping("buscar")
	public String buscar(@RequestParam String buscarPor,
						 @RequestParam String exp,
						 Model model){
		if(!buscarPor.equals("")){
			List<LibroEstadoDTO> libros= servicioLibro.buscarPor(buscarPor, exp);
			model.addAttribute("libros", libros);
		}
		return "buscadorLibroForm";	
	}	
	
	@RequestMapping("aceptarLibro")
	public String aceptarLibro(@RequestParam Integer idLibro,
							   Model model){

		LibroDTO libro = servicioLibro.buscarLibro(idLibro);
		model.addAttribute("libro", libro);
		return "fichaSocioForm";
	}
	
	@RequestMapping("agregarLibro")
	public String agregarLibro(@ModelAttribute("socio") SocioDTO socioDto,
							   @ModelAttribute("libro") LibroDTO libroDto){
		servicioPrestamo.guardarPrestamo(socioDto, libroDto);
		return "redirect:fichaSocio?id="+socioDto.getIdSocio();
	}
	
	@RequestMapping("devolverLibro")
	public String devolverLibro(@RequestParam Integer id,
								@ModelAttribute("socio") SocioDTO socioDto,
								Model model){
		servicioPrestamo.actualizarPrestamo(id);
		return"redirect:fichaSocio?id="+socioDto.getIdSocio();
	}
}
