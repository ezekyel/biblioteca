package com.ezekiel.biblioteca.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.PrestamoDTO;
import com.ezekiel.biblioteca.servicios.ServicioLibro;
import com.ezekiel.biblioteca.servicios.ServicioPrestamo;

@Controller
public class ReportesController {
	
	@Autowired
	private ServicioPrestamo servicioPrestamo;
	
	@Autowired
	private ServicioLibro servicioLibro;
	
	@RequestMapping("reportes")
	public String reportes() {
		return "redirect:librosMasPopulares";
	}
	
	@RequestMapping("librosMasPopulares")
	public String librosMasPopulares(Model model) {
		List<LibroDTO> libros = servicioLibro.buscarLibrosFavoritos(5);
		model.addAttribute("css", "librosMasPopulares");
		model.addAttribute("libros", libros);
		return "librosMasPopulares";
	}
	
	@RequestMapping("prestamosVencidos")
	public String prestamosVencidos(Model model) {
		List<PrestamoDTO> prestamosDto = servicioPrestamo.getAllPrestamosVencidos();
		model.addAttribute("prestamos", prestamosDto);
		model.addAttribute("css", "prestamosVencidos");
		return "prestamosVencidos";
	}
	
	@RequestMapping("reportePorLibro")
	public String reportePorLibro(Model model) {
		model.addAttribute("css", "reportePorLibro");
		return "reportePorLibro";
	}
	
	@RequestMapping("reportePorSocio")
	public String reportePorSocio(Model model) {
		model.addAttribute("css", "reportePorSocio");
		return "reportePorSocio";
	}
	
}
