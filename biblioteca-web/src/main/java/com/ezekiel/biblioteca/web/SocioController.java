package com.ezekiel.biblioteca.web;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.servicios.ServicioSocio;

@Controller
public class SocioController {
	
	private final Logger logger = LoggerFactory.getLogger(SocioController.class);
	
	@Autowired
	private ServicioSocio servicioSocio;
	
	@RequestMapping("registrarSocio")
	public ModelAndView registrarSocio(@ModelAttribute SocioDTO socio){
		logger.info("nuevo socio");
		return new ModelAndView("registrarSocioForm");
	}
	
	@RequestMapping("editarSocio")
	public ModelAndView editarSocio(@RequestParam int id, 
									@ModelAttribute SocioDTO socio){
		logger.info("actulizando socio " + id);
		socio = servicioSocio.getSocioById(id);
		return new ModelAndView("registrarSocioForm", "socioDto", socio);
	}
	
	@RequestMapping("guardarSocio")
	public ModelAndView guardarSocio(@Valid @ModelAttribute SocioDTO socio, BindingResult result){

		if(result.hasErrors()){
			logger.info("error al guardar");
			return new ModelAndView("registrarSocioForm");
		} 
		logger.info("guardado ok");
		servicioSocio.guardarSocio(socio);
		return new ModelAndView("redirect:listarSocios");		
	}
	
	@RequestMapping("borrarSocio")
	public ModelAndView borrarSocio(@RequestParam int id){
		logger.info("borrado " + id);
		servicioSocio.borrarSocio(id);
		return new ModelAndView("redirect:listarSocios");
	} 
	
	@RequestMapping(value = {"listarSocios", "/"})
	public ModelAndView listarSocios(){
		logger.info("listando socios");
		List<SocioDTO> socios = servicioSocio.buscarTodosLosSocios();
		return new ModelAndView("listarSocios", "socios", socios);
	}
}
