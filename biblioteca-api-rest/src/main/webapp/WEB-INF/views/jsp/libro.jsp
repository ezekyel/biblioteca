<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html data-ng-app="AppLibro">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Responsive -->
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="resources/css/bootstrap.css" rel="stylesheet" />
	
<title>Libro</title>
</head>
<body>

	<%@include file="../include/menu.jsp" %>	
	
	<div class="container-fluid">
		<div data-ng-view=""></div>
	</div>	

	<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap.js"></script>
	
	<script type="text/javascript" src="resources/js/angular.min.js"></script>
	<script type="text/javascript" src="resources/js/angular-route.min.js" ></script>
	<script type="text/javascript" src="resources/js/ui-bootstrap-tpls-2.0.0.min.js"></script>
	
	<script type="text/javascript" src="resources/applications/libro/AppLibro.js"></script>
	<script type="text/javascript" src="resources/applications/libro/controller/LibroListaController.js"></script>
	<script type="text/javascript" src="resources/applications/libro/controller/LibroController.js"></script>
	<script type="text/javascript" src="resources/applications/libro/services/LibroService.js"></script>

</body>
</html>