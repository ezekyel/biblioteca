var app = angular.module('AppLibro', ['ngRoute', 'ui.bootstrap']);
// ngRoute: genera el ruteo hacia los partials
// ui.bootstrap : permite trabajar con template

app.config(function ($routeProvider){
	$routeProvider
		// Listado de libros
		.when('/libro', {			
			controller: 'LibroListaController',
			templateUrl: 'resources/applications/libro/partials/listadoLibro.html'
		})
		// Formulario
		.when('/manager-libro/:libroId', {
			title: 'Editar Libro',
			controller: 'LibroController',
			templateUrl: 'resources/applications/libro/partials/registroLibro.html',
			resolve: {
				libro : function(LibroService, $route){
					console.log('en ApLibros');
					
					var libroId = $route.current.params.libroId;
					if(libroId != 0){
						console.log('en != 0 ');
						
						//retornar libro data lleno
					} else {
						console.log('en == 0');
						
						var libro = new Object;
						libro['isbn'] = '';
						libro['titulo'] = '';
						libro['autor'] = '';
						libro['categoria'] = '';
						var data = {};
						data['data'] = libro;
						return data;
					}
				}
			}
		})
		.otherwise ({redirect:'/libro'});
});