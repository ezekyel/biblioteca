app.controller('LibroController', function($scope, LibroService, $http, $location, $rootScope,
										   $routeParams, libro){
	
	var libroId = ($routeParams.libroId) ? parseInt($routeParams.businessId) : 0;
	$rootScope.title = (libroId > 0) ? 'Editar Libro' : 'Registrar Libro';
	$rootScope.buttonText = (libroId > 0) ? 'Actualizar Libro' : 'Registrar Libro';
});