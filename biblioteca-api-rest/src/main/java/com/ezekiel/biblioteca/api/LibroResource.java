package com.ezekiel.biblioteca.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HttpServletBean;

import com.ezekiel.biblioteca.api.exceptions.libro.NotFoundLibroIdException;
import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.response.Response;
import com.ezekiel.biblioteca.commons.response.ResponsePage;
import com.ezekiel.biblioteca.servicios.ServicioLibro;

@RestController
@RequestMapping(value = Uris.LIBROS)
public class LibroResource {

	@Autowired
	ServicioLibro serviLibro;

	@RequestMapping(params = { "page",
			"size" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponsePage<LibroDTO>> getAll(@RequestParam("page") int page,
			@RequestParam("size") int size) {

		ResponsePage<LibroDTO> pageLibro = serviLibro.getPorPagina(page, size);
		pageLibro.setErrorMessage(Response.SUCCESS);
		pageLibro.setMenssage(Response.SUCCESS);

		return new ResponseEntity<ResponsePage<LibroDTO>>(pageLibro, HttpStatus.OK);
	}

	@RequestMapping(value = Uris.ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LibroDTO> getByid(@PathVariable("id") int id) throws NotFoundLibroIdException {

		LibroDTO libro = serviLibro.buscarLibro(id);
		if (libro == null) {
			throw new NotFoundLibroIdException("id:" + id);
		}
		return new ResponseEntity<LibroDTO>(libro, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LibroDTO>> find() {

		List<LibroDTO> libros = serviLibro.buscarTodosLosLibros();
		if (libros.isEmpty()) {
			return new ResponseEntity<List<LibroDTO>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<LibroDTO>>(libros, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<Void> insert(@RequestBody LibroDTO libro, HttpServletBean request) {

		Integer id = serviLibro.guardarLibro(libro);

		if (id < 0) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		HttpHeaders headers = new HttpHeaders();

		try {
			headers.setLocation(new URI(((HttpServletRequest) request).getRequestURL() + Integer.toString(id)));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = Uris.ID, method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public ResponseEntity<Void> update(@PathVariable("id") int id, @RequestBody LibroDTO libro) {
		LibroDTO libroDto = serviLibro.buscarLibro(id);

		if (libroDto == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		libro.setIdLibro(libroDto.getIdLibro());
		serviLibro.actualizarLibro(libro);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = Uris.LIBROS + Uris.ID, method = RequestMethod.DELETE)
	public ResponseEntity<LibroDTO> delete(@PathVariable("id") int id) {
		LibroDTO libro = serviLibro.buscarLibro(id);

		if (libro == null) {
			return new ResponseEntity<LibroDTO>(HttpStatus.NOT_FOUND);
		}

		serviLibro.borrarLibro(id);
		return new ResponseEntity<LibroDTO>(HttpStatus.NO_CONTENT);
	}
}

/*
 * 
 * @RequestMapping(params={"inicio", "size"}, method = RequestMethod.GET,
 * produces = MediaType.APPLICATION_JSON_VALUE) public
 * ResponseEntity<Page<LibroDTO>> getAll(@RequestParam("inicio") int inicio,
 * 
 * @RequestParam("size") int size) throws NotFoundLibroIdException {
 * 
 * Page<LibroDTO> page = serviLibro.getPorPagina(inicio, size);
 * 
 * return new ResponseEntity<Page<LibroDTO>>(page, HttpStatus.OK); }
 * 
 * 
 * @RequestMapping(value = Uris.ID, method = RequestMethod.GET, produces =
 * MediaType.APPLICATION_JSON_VALUE) public ResponseEntity<LibroDTO>
 * getByid(@PathVariable("id") int id) throws NotFoundLibroIdException {
 * 
 * LibroDTO libro = serviLibro.buscarLibro(id); if (libro == null) { throw new
 * NotFoundLibroIdException("id:" + id); } return new
 * ResponseEntity<LibroDTO>(libro, HttpStatus.OK); }
 * 
 * 
 * @RequestMapping(method = RequestMethod.GET, produces =
 * MediaType.APPLICATION_JSON_VALUE) public ResponseEntity<List<LibroDTO>>
 * find() {
 * 
 * List<LibroDTO> libros = serviLibro.buscarTodosLosLibros(); if
 * (libros.isEmpty()) { return new
 * ResponseEntity<List<LibroDTO>>(HttpStatus.NO_CONTENT); } return new
 * ResponseEntity<List<LibroDTO>>(libros, HttpStatus.OK); }
 * 
 * 
 * @RequestMapping(method = RequestMethod.POST, produces = "application/json",
 * consumes = "application/json") public ResponseEntity<Void>
 * insert(@RequestBody LibroDTO libro, HttpServletRequest request) {
 * 
 * Integer id = serviLibro.guardarLibro(libro);
 * 
 * if (id < 0) { return new ResponseEntity<Void>(HttpStatus.CONFLICT); }
 * 
 * HttpHeaders headers = new HttpHeaders();
 * 
 * try { headers.setLocation(new URI(request.getRequestURL() +
 * Integer.toString(id))); } catch (URISyntaxException e) { e.printStackTrace();
 * } return new ResponseEntity<>(headers, HttpStatus.CREATED); }
 * 
 * 
 * @RequestMapping(value = Uris.ID, method = RequestMethod.PUT, produces =
 * "application/json", consumes = "application/json") public
 * ResponseEntity<Void> update(@PathVariable("id") int id, @RequestBody LibroDTO
 * libro) { LibroDTO libroDto = serviLibro.buscarLibro(id);
 * 
 * if (libroDto == null) { return new
 * ResponseEntity<Void>(HttpStatus.NOT_FOUND); }
 * 
 * libro.setIdLibro(libroDto.getIdLibro()); serviLibro.actualizarLibro(libro);
 * return new ResponseEntity<Void>(HttpStatus.CREATED); }
 * 
 * 
 * @RequestMapping(value = Uris.LIBROS + Uris.ID, method = RequestMethod.DELETE)
 * public ResponseEntity<LibroDTO> delete(@PathVariable("id") int id) { LibroDTO
 * libro = serviLibro.buscarLibro(id);
 * 
 * if (libro == null) { return new
 * ResponseEntity<LibroDTO>(HttpStatus.NOT_FOUND); }
 * 
 * serviLibro.borrarLibro(id); return new
 * ResponseEntity<LibroDTO>(HttpStatus.NO_CONTENT); }
 */
