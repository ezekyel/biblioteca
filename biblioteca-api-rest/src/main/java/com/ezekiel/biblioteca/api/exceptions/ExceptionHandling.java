package com.ezekiel.biblioteca.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ezekiel.biblioteca.api.exceptions.libro.AlreadyExistLibroException;
import com.ezekiel.biblioteca.api.exceptions.libro.InvalidLibroFieldException;
import com.ezekiel.biblioteca.api.exceptions.libro.InvalidPageNumberException;
import com.ezekiel.biblioteca.api.exceptions.libro.NotFoundLibroIdException;

@ControllerAdvice
public class ExceptionHandling extends ResponseEntityExceptionHandler{
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundLibroIdException.class, 
					   InvalidPageNumberException.class})
	@ResponseBody
	public ErrorMessge notFoundRequest(Exception exeption){
		ErrorMessge errorMessge = new ErrorMessge(exeption);
		return errorMessge;
	}	
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({InvalidLibroFieldException.class})
	@ResponseBody
	public ErrorMessge badRequest(Exception exception){
		ErrorMessge errorMessge = new ErrorMessge(exception);
		return errorMessge;
	}
		
	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({AlreadyExistLibroException.class})
	@ResponseBody
	public ErrorMessge conflictRequest(Exception exception){
		ErrorMessge errorMessage = new ErrorMessge(exception);
		return errorMessage;
	}		
	
//	@ResponseStatus(HttpStatus.UNAUTHORIZED)
//    @ExceptionHandler({UnauthorizedException.class})
//    public void unauthorized(ApiException exception) {
//
//    }
	
}
