package com.ezekiel.biblioteca.api.exceptions.libro;

public class InvalidPageNumberException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public static final String DESCRIPCION = "Numero de pagina invalido";
	
	public static final int CODE = 404;

	public InvalidPageNumberException() {
		this("");
	}
	
	public InvalidPageNumberException(String detalle) {
		super(DESCRIPCION + ". " + detalle + ". CODE " + CODE);
	}	
}
