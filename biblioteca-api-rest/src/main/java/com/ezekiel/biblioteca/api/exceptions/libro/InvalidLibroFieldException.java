package com.ezekiel.biblioteca.api.exceptions.libro;

public class InvalidLibroFieldException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public static final String DESCRIPCION = "Campo libro vacio o inexistente";
	
	public static final int CODE = 404;

	public InvalidLibroFieldException() {
		this("");
	}
	
	public InvalidLibroFieldException(String detalle) {
		super(DESCRIPCION + ". " + detalle + ". CODE " + CODE);
	}	
}
