package com.ezekiel.biblioteca.api.exceptions.libro;

public class NotFoundLibroIdException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public static final String DESCRIPCION = "No se encontro el idententificador del libro";
	
	public static final int CODE = 404;

	public NotFoundLibroIdException() {
		this("");
	}
	
	public NotFoundLibroIdException(String detalle) {
		super(DESCRIPCION + ". " + detalle + ". CODE " + CODE);
	}	
}
