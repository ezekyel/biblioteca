package com.ezekiel.biblioteca.api;

public class Uris {
	
	public static final String API = "/api";
	
	public static final String ID = "/{id}";
	
	public static final String LIBROS = "/libros";
	
}