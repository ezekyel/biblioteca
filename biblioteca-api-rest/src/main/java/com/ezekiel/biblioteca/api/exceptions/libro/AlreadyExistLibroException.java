package com.ezekiel.biblioteca.api.exceptions.libro;

public class AlreadyExistLibroException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public static final String DESCRIPCION = "Libro existente";
	
	public static final int CODE = 404;

	public AlreadyExistLibroException() {
		this("");
	}
	
	public AlreadyExistLibroException(String detalle) {
		super(DESCRIPCION + ". " + detalle + ". CODE " + CODE);
	}	
}
