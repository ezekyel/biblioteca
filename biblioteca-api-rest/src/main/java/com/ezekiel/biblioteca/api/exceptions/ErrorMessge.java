package com.ezekiel.biblioteca.api.exceptions;

public class ErrorMessge {
	
	private String error;
	
	private String descripcion;	

	public ErrorMessge(Exception exception) {
		this(exception.getClass().getSimpleName(), exception.getMessage());
	}

	public ErrorMessge(String error, String descripcion) {
		this.error = error;
		this.descripcion = descripcion;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
