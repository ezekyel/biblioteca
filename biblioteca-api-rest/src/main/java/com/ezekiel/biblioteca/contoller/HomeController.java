package com.ezekiel.biblioteca.contoller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	@RequestMapping(value="/")
	public ModelAndView redireccionHome(){
		return new ModelAndView("home");				
	}
	
	@RequestMapping(value="libroHome")
	public ModelAndView redireccionLibro(){
		return new ModelAndView("/jsp/libro");				
	}
	
}
