package com.ezekiel.biblioteca.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.commons.filter.FiltroSocio;
import com.ezekiel.biblioteca.dao.SocioDAO;
import com.ezekiel.biblioteca.domain.Socio;
import com.ezekiel.biblioteca.servicios.ServicioSocio;

@Service
@Transactional
public class ServicioSocioImpl implements ServicioSocio {

	@Autowired
	private SocioDAO socioDao;

	@Autowired
	private DozerBeanMapper dozerBean;

	public ServicioSocioImpl() {
	}

	@Override
	public Integer guardarSocio(SocioDTO socioDto) {
		Socio socio = socioDao.save(dozerBean.map(socioDto, Socio.class));
		return socio.getIdSocio();
	}

	@Override
	public void borrarSocio(Integer id) {
		socioDao.delete(socioDao.find(id));
	}

	@Override
	public List<SocioDTO> buscarTodosLosSocios() {
		List<Socio> libros = socioDao.getAll();
		List<SocioDTO> librosDto = new ArrayList<>();

		if (libros != null) {
			for (Socio libro : libros) {
				librosDto.add(dozerBean.map(libro, SocioDTO.class));
			}
		}
		return librosDto;
	}

	@Override
	public SocioDTO getSocioByDni(Integer dni) {
		FiltroSocio filtro = new FiltroSocio();
		filtro.setDni(dni);
		Socio socio = (Socio) socioDao.getSocioByFilter(filtro);
		return socio != null ? dozerBean.map(socio, SocioDTO.class) : null;
	}

	@Override
	public SocioDTO getSocioById(Integer id) {
		FiltroSocio filtro = new FiltroSocio();
		filtro.setIdSocio(id);
		Socio socio = (Socio) socioDao.getSocioByFilter(filtro);
		return socio != null ? dozerBean.map(socio, SocioDTO.class) : null;
	}

	@Override
	public List<SocioDTO> buscarPorFiltro(FiltroSocio filtro) {
		List<Socio> socios = socioDao.getAll(filtro);
		List<SocioDTO> socioDto = new ArrayList<>();
		for (Socio socio : socios) {
			socioDto.add(dozerBean.map(socio, SocioDTO.class));
		}
		return socioDto;
	}

	@Override
	public SocioDTO buscarSocio(int id) {
		Socio socio = socioDao.find(id);
		return socio != null? dozerBean.map(socio, SocioDTO.class) : null;
	}	
	
	@Override
	public Integer actualizarSocio(SocioDTO socioDto) {
		return socioDao.update((dozerBean.map(socioDto, Socio.class))).getIdSocio();
	}
}
