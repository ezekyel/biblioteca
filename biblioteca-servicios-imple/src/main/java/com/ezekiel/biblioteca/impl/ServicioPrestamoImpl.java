package com.ezekiel.biblioteca.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.PrestamoDTO;
import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.commons.filter.FiltroPrestamo;
import com.ezekiel.biblioteca.commons.utils.FechaUtils;
import com.ezekiel.biblioteca.dao.PrestamoDAO;
import com.ezekiel.biblioteca.domain.Libro;
import com.ezekiel.biblioteca.domain.Prestamo;
import com.ezekiel.biblioteca.domain.Socio;
import com.ezekiel.biblioteca.servicios.ServicioPrestamo;


@Service
@Transactional
public class ServicioPrestamoImpl implements ServicioPrestamo {
	
	@Autowired
	private PrestamoDAO prestamoDao;
		
	@Autowired
	private DozerBeanMapper dozerBean;

	@Override
	public void guardarPrestamo(SocioDTO socioDto, LibroDTO libroDto) {
		
		Socio socio = socioDto != null ? dozerBean.map(socioDto, Socio.class) : null;
		Libro libro = libroDto != null ? dozerBean.map(libroDto, Libro.class) : null;

		Prestamo prestamo = new Prestamo();
		prestamo.setSocio(socio);
		prestamo.setLibro(libro);
		prestamo.setFechaPrestamo(FechaUtils.getFechaHoy());
		prestamo.setFechaDevolucion(FechaUtils.sumarDiasFechaHoy(7));
		prestamo.setEstado("prestado");
		
		prestamoDao.create(prestamo);		
	}

	@Override
	public void borrarPrestamo(PrestamoDTO prestamDto) {
		prestamoDao.delete(dozerBean.map(prestamDto, Prestamo.class));		
	}

	@Override
	public void actualizarPrestamo(Integer idPrestamo) {
		Prestamo prestamo = prestamoDao.find(idPrestamo);
		prestamo.setEstado("disponible");
		prestamoDao.update(prestamo);		
	}

	@Override
	public List<PrestamoDTO> getPrestamos(SocioDTO socioDto) {
		FiltroPrestamo filtro = new FiltroPrestamo();
		filtro.setIdSocio(socioDto.getIdSocio());
		filtro.setEstado("prestado");
		
		List<Prestamo> prestamos = (List<Prestamo>) prestamoDao.getAll(filtro);
		List<PrestamoDTO> lista = new ArrayList<>();		
		
		if(prestamos.size() > 0){
			for (Prestamo dto : prestamos) {
				lista.add(dozerBean.map(dto, PrestamoDTO.class));
			}
		}
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrestamoDTO> getPrestamosVencidos(SocioDTO socioDto) {
		Socio socio = dozerBean.map(socioDto, Socio.class);
		List<Prestamo> prestamos = prestamoDao.getPrestamosVencidos(FechaUtils.getFechaHoy(), socio);
		
		if(prestamos.size() > 0){
			return (List<PrestamoDTO>) dozerBean.map(prestamos, PrestamoDTO.class);
		} else {
			return null;
		}
	}

	@Override
	public List<PrestamoDTO> getHistorialPrestamos(SocioDTO socioDto) {
		return null;
	}

	@Override
	public List<PrestamoDTO> getAllPrestamosVencidos() {
		List<Prestamo> prestamos = prestamoDao.getAllPrestamosVencidos();
		List<PrestamoDTO> lista = new ArrayList<>();
		
		if(prestamos.size() > 0 ){
			for (Prestamo prestamo : prestamos) {
				lista.add(dozerBean.map(prestamo, PrestamoDTO.class));				
			}
		}
		return lista;
	}

}
