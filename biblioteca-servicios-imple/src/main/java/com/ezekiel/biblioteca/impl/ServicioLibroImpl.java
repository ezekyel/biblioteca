package com.ezekiel.biblioteca.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.LibroEstadoDTO;
import com.ezekiel.biblioteca.commons.filter.FiltroLibro;
import com.ezekiel.biblioteca.commons.response.Response;
import com.ezekiel.biblioteca.commons.response.ResponsePage;
import com.ezekiel.biblioteca.dao.LibroDAO;
import com.ezekiel.biblioteca.dao.PrestamoDAO;
import com.ezekiel.biblioteca.domain.Libro;
import com.ezekiel.biblioteca.domain.Prestamo;
import com.ezekiel.biblioteca.servicios.ServicioLibro;

@Service
@Transactional
public class ServicioLibroImpl implements ServicioLibro {

	@Autowired
	private LibroDAO libroDao;
	
	@Autowired
	private PrestamoDAO prestamoDao;
	
	@Autowired
	private DozerBeanMapper dozerBean;

	public ServicioLibroImpl() {
	}

	@Override
	public Integer guardarLibro(LibroDTO libroDto) {
		
		FiltroLibro filtro = dozerBean.map(libroDto, FiltroLibro.class);

		if (!buscarPorFiltro(filtro).isEmpty()) {
			return -1;
		}
		
		return libroDao.save(dozerBean.map(libroDto, Libro.class)).getIdLibro();		
	}

	@Override
	public void borrarLibro(int id) {
		libroDao.delete(libroDao.find(id));
	}

	@Override
	public List<LibroDTO> buscarTodosLosLibros() {
		List<LibroDTO> librosDto = new ArrayList<>();

		List<Libro> libros = libroDao.getAll();
		
		if(libros != null){
			for (Libro libro : libros) {
				librosDto.add(dozerBean.map(libro, LibroDTO.class));
			}			
		}		
		return librosDto;
	}
	
	@Override
	public LibroDTO buscarLibro(int id) {
		Libro libro = libroDao.find(id);
		return libro != null ? dozerBean.map(libro, LibroDTO.class): null;
	}

	@Override
	public List<LibroDTO> buscarPorFiltro(FiltroLibro filtro) {	
		List<Libro> libros = libroDao.getAll(filtro);
		List<LibroDTO> libroDto = new ArrayList<>();
		
		for (Libro libro : libros) {
			libroDto.add(dozerBean.map(libro, LibroDTO.class));
		}
		return libroDto;
	}

	public List<LibroEstadoDTO> buscarPor(String key, String value){
		FiltroLibro filtro = new FiltroLibro();
		List<LibroEstadoDTO> lista = new ArrayList<>();
		
		if(key == null)	return null;
		
		if(key.equals("isbn"))
			filtro.setIsbn(value);
		if(key.equals("autor"))
			filtro.setAutor(value);
		if(key.equals("titulo"))
			filtro.setTitulo(value);
		if(key.equals("isbn"))
			filtro.setCategoria(value);
		
		List<Libro> libros = libroDao.getAll(filtro);
		
		if(libros.size() <= 0) {
			return null;
		}

		for (Libro libro : libros) {
			LibroEstadoDTO libroEstado = dozerBean.map(libro, LibroEstadoDTO.class);
			libroEstado.setDisponible(isDisponible(libro));
			lista.add(libroEstado);
		}
		
		return lista;
	}

	@Override
	public List<LibroDTO> buscarLibrosFavoritos(Integer max) {
		List<Libro> libros = libroDao.getTopLibros(max);
		List<LibroDTO> lista = new ArrayList<>();
		
		if(libros.size() > 0){
			for (Libro libro : libros) {
				lista.add(dozerBean.map(libro, LibroDTO.class));
			}
		}
		return lista;
	}

	@Override
	public boolean isDisponible(Libro libro) {
		Prestamo pestamo = prestamoDao.getUltimoPrestamo(dozerBean.map(libro, Libro.class));
		if(pestamo == null || pestamo.getEstado().equals("disponible")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Integer actualizarLibro(LibroDTO libroDto) {
		return libroDao.update((dozerBean.map(libroDto, Libro.class))).getIdLibro();
	}

	@Override
	public ResponsePage<LibroDTO> getPorPagina(Integer page, Integer size) {		
		ResponsePage<LibroDTO> pageLibro = new ResponsePage<>(page, size, libroDao.getAllCount());	
		List<Libro> libros = libroDao.getAll(pageLibro.firstItem(), size);
		List<LibroDTO> librosDto = new ArrayList<>();
		
		if(libros.size() > 0){
			for (Libro libro : libros) {
				librosDto.add(dozerBean.map(libro, LibroDTO.class));
			}
		}
		
		pageLibro.setLista(librosDto);
		return pageLibro;
	}
}
