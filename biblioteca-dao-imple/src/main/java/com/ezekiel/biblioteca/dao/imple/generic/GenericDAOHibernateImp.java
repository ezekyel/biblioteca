package com.ezekiel.biblioteca.dao.imple.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekiel.biblioteca.dao.generic.GenericDAO;

public abstract class GenericDAOHibernateImp<ENTITY, ID extends Serializable>
	implements GenericDAO<ENTITY, ID>{
	
	private Class<ENTITY> type;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public GenericDAOHibernateImp() {
		this.type = (Class<ENTITY>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Criteria getCriteria(){
		return getSession().createCriteria(type);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENTITY create(ENTITY entity) {
		return (ENTITY) getSession().save(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENTITY find(ID id) {
		return (ENTITY) getSession().get(type, id);
	}

	@Override
	public ENTITY update(ENTITY entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENTITY save(ENTITY entity) {
		return (ENTITY) getSession().merge(entity);
	}

	@Override
	public void delete(ENTITY entity) {
		getSession().delete(entity);
	}

	public List<ENTITY> getAll() {
		Criteria criteria = getSession().createCriteria(type);
		return list(criteria);
	}
	
	public List<ENTITY> getAllPaginated(int inicio, int size) {
		Criteria criteria = getSession().createCriteria(type);
		criteria.setFirstResult(inicio);
		criteria.setMaxResults(size);
		return list(criteria);
	}
	
	public int getAllCount() {
		Criteria criteria = getSession().createCriteria(type);
		criteria.setProjection(Projections.rowCount());
		return ((Number) uniqueResult(criteria)).intValue();
	}
	
	@SuppressWarnings("unchecked")
	protected ENTITY uniqueResult(Criteria criteria) {
		return (ENTITY) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	protected List<ENTITY> list(Criteria criteria) {
		return (List<ENTITY>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	protected List<ENTITY> list(Query query) {
		return (List<ENTITY>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	protected ENTITY uniqueResult(Query query) {
		return (ENTITY) query.uniqueResult();
	}
}