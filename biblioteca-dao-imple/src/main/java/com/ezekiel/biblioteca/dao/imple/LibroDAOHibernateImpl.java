package com.ezekiel.biblioteca.dao.imple;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ezekiel.biblioteca.commons.filter.FiltroLibro;
import com.ezekiel.biblioteca.dao.LibroDAO;
import com.ezekiel.biblioteca.dao.imple.generic.GenericFilterableDAOHibernateImpl;
import com.ezekiel.biblioteca.domain.Libro;

@Repository
public class LibroDAOHibernateImpl 
	extends GenericFilterableDAOHibernateImpl<Libro, FiltroLibro, Integer>
	implements LibroDAO {
	
	@Override
	public Libro getLibroByFilter(FiltroLibro filtro) {
		Criteria criteria = getCriteria();
		addFilters(filtro, criteria);
		return uniqueResult(criteria);
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Libro> getTopLibros(Integer max) {
		String consulta = "SELECT libro FROM Prestamo P GROUP BY libro ORDER BY count(libro) desc";		
		Query query = getSession().createQuery(consulta);
		query.setMaxResults(max);		
		List<Libro> libros = query.list();
		return libros;
	}	

	@Override
	protected void addFilters(FiltroLibro filtro, Criteria criteria) {
		if(filtro == null) return;
		
		if(filtro.getIdLibro() != null)
			criteria.add(Restrictions.idEq(filtro.getIdLibro()));
		
		if(filtro.getIsbn() != null)
			criteria.add(Restrictions.like("isbn", "%" + filtro.getIsbn() + "%"));
		
		if(filtro.getTitulo() != null)
			criteria.add(Restrictions.like("titulo", "%" + filtro.getTitulo() + "%"));
		
		if(filtro.getAutor() != null)
			criteria.add(Restrictions.like("autor", "%" + filtro.getAutor() + "%"));
		
		if(filtro.getCategoria() != null)
			criteria.add(Restrictions.like("categoria", "%" + filtro.getCategoria() + "%"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Libro> getAll(Integer desde, Integer hasta) {
		Criteria criteria = getSession().createCriteria(Libro.class);
		criteria.setFirstResult(desde);
		criteria.setMaxResults(hasta);
		return criteria.list();
	}
}
