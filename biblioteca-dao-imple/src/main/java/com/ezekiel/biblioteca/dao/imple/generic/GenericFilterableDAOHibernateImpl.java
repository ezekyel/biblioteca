package com.ezekiel.biblioteca.dao.imple.generic;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

import com.ezekiel.biblioteca.commons.filter.Filtro;
import com.ezekiel.biblioteca.dao.generic.GenericFilterableDAO;

public abstract class GenericFilterableDAOHibernateImpl<ENTITY, FILTER extends Filtro, ID extends Serializable>
extends GenericDAOHibernateImp<ENTITY, ID>
implements GenericFilterableDAO<ENTITY, FILTER, ID>{
	
	@SuppressWarnings("unchecked")
	public List<ENTITY> getAll(FILTER filtro){
		Criteria criteria = getCriteria();
		addFilters(filtro, criteria);
		return (List<ENTITY>) criteria.list();
	}
	
	protected abstract void addFilters(FILTER filtro, Criteria criteria);
}
