package com.ezekiel.biblioteca.dao.imple;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ezekiel.biblioteca.commons.filter.FiltroSocio;
import com.ezekiel.biblioteca.dao.SocioDAO;
import com.ezekiel.biblioteca.dao.imple.generic.GenericFilterableDAOHibernateImpl;
import com.ezekiel.biblioteca.domain.Socio;

@Repository
public class SocioDAOHibernateImpl
	extends GenericFilterableDAOHibernateImpl<Socio, FiltroSocio, Integer>
	implements SocioDAO{
	
	@Override
	public Socio getSocioByFilter(FiltroSocio filtro) {
		Criteria criteria = getCriteria();
		addFilters(filtro, criteria);
		return uniqueResult(criteria);
	}
	
	@Override
	protected void addFilters(FiltroSocio filtro, Criteria criteria) {
		if(filtro == null) return;
		
		if(filtro.getIdSocio() != null)
			criteria.add(Restrictions.idEq(filtro.getIdSocio()));
		
		if(filtro.getDni() != null)
			criteria.add(Restrictions.eq("dni", filtro.getDni()));
		
		if(filtro.getNombre() != null)
			criteria.add(Restrictions.like("nombre", filtro.getNombre()));
		
		if(filtro.getApellido() != null)
			criteria.add(Restrictions.like("apellido", filtro.getApellido()));
		
		if(filtro.getDireccion() != null)
			criteria.add(Restrictions.like("direccion", filtro.getDireccion()));
		
		if(filtro.getTelefono() != null)
			criteria.add(Restrictions.like("telefono", filtro.getTelefono()));
		
		if(filtro.getEmail() != null)
			criteria.add(Restrictions.like("email", filtro.getEmail()));		
		
	}
}
