package com.ezekiel.biblioteca.dao.imple;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ezekiel.biblioteca.commons.filter.FiltroPrestamo;
import com.ezekiel.biblioteca.commons.utils.FechaUtils;
import com.ezekiel.biblioteca.dao.PrestamoDAO;
import com.ezekiel.biblioteca.dao.imple.generic.GenericFilterableDAOHibernateImpl;
import com.ezekiel.biblioteca.domain.Libro;
import com.ezekiel.biblioteca.domain.Prestamo;
import com.ezekiel.biblioteca.domain.Socio;

@Repository
public class PrestamoDAOHibernateImpl 
	extends GenericFilterableDAOHibernateImpl<Prestamo, FiltroPrestamo, Integer>
	implements PrestamoDAO {
	
	@Override
	public List<Prestamo> getPrestamosVencidos(Date fecha, Socio socio) {
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.gt("fechaDevolucion", fecha));
		criteria.add(Restrictions.eq("idSocio", socio.getIdSocio()));
		criteria.add(Restrictions.eq("estado", "prestado"));
		return list(criteria);
	}
	
	public List<Prestamo> getAllPrestamosVencidos(){
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.gt("fechaDevolucion", FechaUtils.getFechaHoy()));
		criteria.add(Restrictions.eq("estado", "prestado"));
		return list(criteria);
	}
	
	@Override
	public Prestamo getUltimoPrestamo(Libro libro) {
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("libro", libro))
			    .addOrder(Order.desc("fechaPrestamo"))
		        .setMaxResults(1);
		Object prestamo = criteria.uniqueResult();
		return (Prestamo) prestamo;
	}

	@Override
	protected void addFilters(FiltroPrestamo filtro, Criteria criteria) {
		if(filtro == null) return;
		
		if(filtro.getIdPrestamo() != null)
			criteria.add(Restrictions.idEq(filtro.getIdPrestamo()));

		if(filtro.getFechaPrestamo() != null)
			criteria.add(Restrictions.eq("fechaPrestamo", filtro.getFechaPrestamo()));

		if(filtro.getFechaDevolucion() != null)
			criteria.add(Restrictions.eq("fechaDevolucion", filtro.getFechaDevolucion()));

		if(filtro.getEstado() != null)
			criteria.add(Restrictions.eq("estado", filtro.getEstado()));

		if(filtro.getIdSocio() != null)
			criteria.add(Restrictions.eq("socio.idSocio", filtro.getIdSocio()));

		if(filtro.getIdLibro() != null)
			criteria.add(Restrictions.eq("libro.idLibro", filtro.getIdLibro()));
		
	}
}