package com.ezekiel.biblioteca.servicios;

import java.util.List;

import com.ezekiel.biblioteca.commons.dto.SocioDTO;
import com.ezekiel.biblioteca.commons.filter.FiltroSocio;

public interface ServicioSocio {
	
	public Integer guardarSocio(SocioDTO socioiDto);
	
	public void borrarSocio(Integer id);
	
	public SocioDTO getSocioByDni(Integer dni);
	
	public SocioDTO getSocioById(Integer id);
	
	public List<SocioDTO> buscarTodosLosSocios();
	
	public List<SocioDTO> buscarPorFiltro(FiltroSocio filtro);
	
	public SocioDTO buscarSocio(int id);

	public Integer actualizarSocio(SocioDTO socioDto);
}