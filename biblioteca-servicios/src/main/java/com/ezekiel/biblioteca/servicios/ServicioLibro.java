package com.ezekiel.biblioteca.servicios;

import java.util.List;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.LibroEstadoDTO;
import com.ezekiel.biblioteca.commons.filter.FiltroLibro;
import com.ezekiel.biblioteca.commons.response.ResponsePage;
import com.ezekiel.biblioteca.domain.Libro;

public interface ServicioLibro {
	
	public Integer guardarLibro(LibroDTO libroDto);
	
	public Integer actualizarLibro(LibroDTO libroDto);
	
	public void borrarLibro(int id);
	
	public LibroDTO buscarLibro(int id);
	
	public List<LibroDTO> buscarTodosLosLibros();
	
	public List<LibroDTO> buscarPorFiltro(FiltroLibro filtro);
	
	public List<LibroEstadoDTO> buscarPor(String key, String value);
	
	public List<LibroDTO> buscarLibrosFavoritos(Integer max);
	
	public boolean isDisponible(Libro libro);

	public ResponsePage<LibroDTO> getPorPagina(Integer page, Integer size);
}
