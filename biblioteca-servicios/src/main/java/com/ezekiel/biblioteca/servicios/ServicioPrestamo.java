package com.ezekiel.biblioteca.servicios;

import java.util.List;

import com.ezekiel.biblioteca.commons.dto.LibroDTO;
import com.ezekiel.biblioteca.commons.dto.PrestamoDTO;
import com.ezekiel.biblioteca.commons.dto.SocioDTO;

public interface ServicioPrestamo {
	
	public void guardarPrestamo(SocioDTO socioDto, LibroDTO libroDto);
	
	public void borrarPrestamo(PrestamoDTO prestamoDto);
	
	public void actualizarPrestamo(Integer idPrestamo);
	
	public List<PrestamoDTO> getPrestamos(SocioDTO socioDto);
	
	public List<PrestamoDTO> getPrestamosVencidos(SocioDTO socioDto);
	
	public List<PrestamoDTO> getHistorialPrestamos(SocioDTO socioDto);
	
	public List<PrestamoDTO> getAllPrestamosVencidos();
}
