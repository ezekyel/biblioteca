package com.ezekiel.biblioteca.dao;

import java.util.List;

import com.ezekiel.biblioteca.commons.filter.FiltroSocio;
import com.ezekiel.biblioteca.dao.generic.GenericFilterableDAO;
import com.ezekiel.biblioteca.domain.Socio;

public interface SocioDAO extends GenericFilterableDAO<Socio, FiltroSocio, Integer>{
	
	public Socio getSocioByFilter(FiltroSocio filtro);
	
	public int getAllCount();
	
	public List<Socio> getAll();
	
}
