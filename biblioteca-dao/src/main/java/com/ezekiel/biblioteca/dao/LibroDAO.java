package com.ezekiel.biblioteca.dao;

import java.util.List;

import com.ezekiel.biblioteca.commons.filter.FiltroLibro;
import com.ezekiel.biblioteca.dao.generic.GenericFilterableDAO;
import com.ezekiel.biblioteca.domain.Libro;

public interface LibroDAO extends GenericFilterableDAO<Libro, FiltroLibro, Integer> {
	
	public Libro getLibroByFilter(FiltroLibro filtro);
	
	public List<Libro> getTopLibros(Integer max);
	
	public int getAllCount();
	
	public List<Libro> getAll();
	
	public List<Libro> getAll(Integer desde, Integer hasta);

}
