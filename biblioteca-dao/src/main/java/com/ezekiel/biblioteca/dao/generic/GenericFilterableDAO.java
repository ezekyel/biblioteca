package com.ezekiel.biblioteca.dao.generic;

import java.io.Serializable;
import java.util.List;

import com.ezekiel.biblioteca.commons.filter.Filtro;

public interface GenericFilterableDAO<ENTITY, FILTER extends Filtro, ID extends Serializable>
extends GenericDAO<ENTITY, ID>{
	
	public abstract List<ENTITY> getAll(FILTER filtro);
	
}
