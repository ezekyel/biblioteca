package com.ezekiel.biblioteca.dao.generic;

import java.io.Serializable;

public interface GenericDAO<ENTITY, ID extends Serializable> {
	
	public ENTITY create(ENTITY entity);
	
	public ENTITY find(ID id);
	
	public ENTITY update(ENTITY entity);
	
	public ENTITY save(ENTITY entity);
	
	public void delete(ENTITY entity);
}
