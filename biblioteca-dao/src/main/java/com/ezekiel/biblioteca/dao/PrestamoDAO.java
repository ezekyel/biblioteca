package com.ezekiel.biblioteca.dao;

import java.util.Date;
import java.util.List;

import com.ezekiel.biblioteca.commons.filter.FiltroPrestamo;
import com.ezekiel.biblioteca.dao.generic.GenericFilterableDAO;
import com.ezekiel.biblioteca.domain.Libro;
import com.ezekiel.biblioteca.domain.Prestamo;
import com.ezekiel.biblioteca.domain.Socio;

public interface PrestamoDAO extends GenericFilterableDAO<Prestamo, FiltroPrestamo, Integer>{
	
	public List<Prestamo> getPrestamosVencidos(Date fecha, Socio socio);
	
	public List<Prestamo> getAllPrestamosVencidos();
	
	public Prestamo getUltimoPrestamo(Libro libro);
	
	public int getAllCount();
	
	public List<Prestamo> getAll();
}
