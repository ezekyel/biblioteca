package com.ezekiel.biblioteca.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PRESTAMO")
public class Prestamo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idPrestamo")
	private int idPrestamo;	
	
	@Column(name="fechaPrestamo")
	@Temporal(TemporalType.DATE)
	private Date fechaPrestamo;
	
	@Column(name="fechaDevolucion")
	@Temporal(TemporalType.DATE)
	private Date fechaDevolucion;
	
	@Column(name="estado")
	private String estado;
	
	@ManyToOne
	@JoinColumn(name="socio")	
	private Socio socio;
	
	@ManyToOne
	@JoinColumn(name="libro")	
	private Libro libro;
	
	public Prestamo() {}

	public Prestamo(Socio socio,
			Libro libro,			 
			Date fechaPrestamo, 
			Date fechaDevolucion, 
			String estado) {
		this.libro = libro;
		this.socio = socio;
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fechaDevolucion;
		this.estado = estado;
	}

	public int getIdPrestamo() {
		return idPrestamo;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Date getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(Date fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}	

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
}
