package com.ezekiel.biblioteca.domain;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = builSessionFactory();
	
	private static SessionFactory builSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory(
							new StandardServiceRegistryBuilder().configure().build());
		} catch (Throwable e) {
			System.err.println("Error alcrear SessionFactory." + e);
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}